package com.example.aop_part3_chapter07

import retrofit2.Call
import retrofit2.http.GET

interface HouseService {

    @GET("/v3/027e2f47-8306-416d-a6bd-ae564b2d3ee2")
    fun getHouseList(): Call<HouseDto>

}